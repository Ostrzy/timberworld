import { writeFileSync } from "fs"

import { generateMap } from "./generateMap"
import { exportMap } from "./exportMap"
import { Random } from "./common/random"

const SIZE = 128

// Interesting seeds: 123456723843, 12345672389743, 867874564, Ostrzy

Random.seed("Ostrzy")

const generatedMap = generateMap({ size: SIZE })
const exportedMap = exportMap(generatedMap)
const mapJSON = JSON.stringify(exportedMap)

writeFileSync("./output.json", mapJSON)
