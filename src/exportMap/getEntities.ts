import { v4 as uuidv4 } from "uuid"

import {
  BuildingEntityType,
  Orientation,
  EntityType,
  TreeEntityType,
  BuildingEntityTypes,
  TreeEntityTypes,
} from "../common/constants"
import { isOneOfEntityTypes } from "../common/utils"
import { Pointable } from "../common/types"
import { Random } from "../common/random"
import { GenerationMap } from "../generateMap"

import { BuildingEntity, NaturalResourceEntity, Entity } from "./types"

function getBuildingEntity(
  entityType: BuildingEntityType,
  { x, y }: Pointable,
  height: number,
  orientation: Orientation
): BuildingEntity {
  return {
    Id: uuidv4(),
    TemplateName: entityType,
    Components: {
      BlockObject: {
        Coordinates: {
          X: x,
          Y: y,
          Z: height,
        },
        Orientation: {
          Value: orientation,
        },
      },
      Constructible: {
        Finished: true,
      },
      ActivableBuilding: {
        IsOn: true,
      },
      ConstructionSite: {
        BuildTimeProgressInHoursKey: 1,
      },
      "Inventory:ConstructionSite": {
        Storage: {
          Goods: [],
        },
      },
    },
  }
}

const MATURE_TREE_ENTITY_TYPES = [
  EntityType.MapleMature,
  EntityType.PineMature,
  EntityType.BirchMature,
]

function getTreeEntity(
  entityType: TreeEntityType,
  { x, y }: Pointable,
  height: number
): NaturalResourceEntity {
  const entity: NaturalResourceEntity = {
    Id: uuidv4(),
    TemplateName: entityType,
    Components: {
      BlockObject: {
        Coordinates: {
          X: x,
          Y: y,
          Z: height,
        },
        Orientation: {
          Value: Orientation.Bottom,
        },
      },
      NaturalResource: {
        SpecificationId: entityType,
        CoordinatesOffset: {
          X: Random.inRange(0.3, 0.75),
          Y: Random.inRange(0.3, 0.75),
        },
        Rotation: Random.inRange(0, 360),
        DiameterScale: Random.inRange(0.8, 1.2),
        HeightScale: Random.inRange(0.8, 1.2),
      },
    },
  }

  // For non-mature trees we need to set NextStageTime for them to actually grow
  if (!MATURE_TREE_ENTITY_TYPES.includes(entityType)) {
    entity.Components.NaturalResource.NextStageTime = Random.inRange(1, 11)
  }

  return entity
}

export function getEntities(gameMap: GenerationMap): Entity[] {
  const entities: Entity[] = []
  gameMap.eachCell((cell, coordinates) => {
    if (!cell.entity) {
      return
    }

    if (isOneOfEntityTypes(cell.entity, BuildingEntityTypes)) {
      entities.push(getBuildingEntity(cell.entity, coordinates, cell.height, Orientation.Top))
    }

    if (isOneOfEntityTypes(cell.entity, TreeEntityTypes)) {
      entities.push(getTreeEntity(cell.entity, coordinates, cell.height))
    }
  })
  return entities
}
