import { GenerationMap } from "../generateMap"
import { GameMap } from "./types"
import { getEntities } from "./getEntities"
import { getMapSingleton } from "./getMapSingleton"

// Take a map and transform it into an object representing final JSON to be exported
export function exportMap(gameMap: GenerationMap): GameMap {
  return {
    Singletons: {
      Map: getMapSingleton(gameMap),
    },
    Entities: getEntities(gameMap),
  }
}
