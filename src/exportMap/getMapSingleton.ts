import { MapSingleton } from "./types"
import { GenerationMap } from "../generateMap"
import { Terrain } from "../common/constants"

function encodeNumber(num: number): string {
  return ("0" + num.toString(16)).substr(-2)
}

// Takes gameMap and returns heights representation as a string
function getHeightMap(gameMap: GenerationMap): string {
  let heights = ""
  gameMap.eachCell((cell) => {
    let height = cell.height
    if (cell.terrain === Terrain.Water) {
      // If we're dealing with water we need to lower the terrain below surface level
      height -= 1
    }
    heights += encodeNumber(height)
  })
  return heights
}

// Takes gameMap and returns water heights representation as a string
function getWaterHeightMap(gameMap: GenerationMap): string {
  let heights = ""
  gameMap.eachCell((cell) => {
    if (cell.terrain === Terrain.Water) {
      heights += encodeNumber(cell.height)
    } else {
      // If we're dealing with anything but water, water should be turned off
      heights += encodeNumber(0)
    }
  })
  return heights
}

// Takes gameMap and returns desert cells representation as a string
function getDesertMap(gameMap: GenerationMap): string {
  let cells = ""
  gameMap.eachCell((cell) => {
    cells += cell.terrain === Terrain.Desert ? "1" : "0"
  })
  return cells
}

export function getMapSingleton(gameMap: GenerationMap): MapSingleton {
  const mapSize = {
    X: gameMap.size,
    Y: gameMap.size,
  }

  return {
    TerrainMap: {
      Size: mapSize,
      Heights: getHeightMap(gameMap),
    },
    WaterMap: {
      Size: mapSize,
      Heights: getWaterHeightMap(gameMap),
    },
    DesertMap: {
      Size: mapSize,
      Cells: getDesertMap(gameMap),
    },
  }
}
