import { EntityType, TreeEntityType, Orientation } from "../common/constants"

export type Entity = {
  Id: string // Random UUID
  TemplateName: EntityType
  Components: {
    BlockObject: {
      Coordinates: {
        X: number
        Y: number
        Z: number
      }
      Orientation: {
        Value: Orientation
      }
    }
  }
}

export type BuildingEntity = Entity & {
  Components: {
    Constructible: {
      Finished: true
    }
    ActivableBuilding: {
      IsOn: true
    }
    ConstructionSite: {
      BuildTimeProgressInHoursKey: 1
    }
    "Inventory:ConstructionSite": {
      Storage: {
        Goods: []
      }
    }
  }
}

export type NaturalResourceEntity = Entity & {
  Components: {
    NaturalResource: {
      SpecificationId: TreeEntityType
      NextStageTime?: number // 1 - 11
      CoordinatesOffset: {
        X: number // 0.3 - 0.75
        Y: number // 0.3 - 0.75
      }
      Rotation: number // 0 - 360
      DiameterScale: number // 0.8 - 1.2
      HeightScale: number // 0.8 - 1.2
    }
  }
}

type MapSize = {
  X: number
  Y: number
}

export type MapSingleton = {
  TerrainMap: {
    Size: MapSize
    Heights: string
  }
  WaterMap: {
    Size: MapSize
    Heights: string
  }
  DesertMap: {
    Size: MapSize
    Cells: string
  }
}

export interface GameMap {
  Singletons: {
    Map: MapSingleton
  }
  Entities: Entity[]
}
