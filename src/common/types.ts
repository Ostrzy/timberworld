export interface Pointable {
  x: number
  y: number
}
