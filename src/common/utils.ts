import { EntityType } from "./constants"

// This function checks whether a given entity type is one of our entity type arrays, like
// TreeEntityTypes. We can't do it by simply calling TreeEntityTypes.includes(entityType) because
// TreeEntityTypes is const and therefore its member types are narrower then EntityType[]. With this
// helper method we also have a type guard for basically free.
export function isOneOfEntityTypes<T extends readonly any[]>(
  entityType: EntityType,
  entityTypes: T
): entityType is T[number] {
  return entityTypes.includes(entityType)
}
