import seedrandom, { prng } from "seedrandom"
import SimplexNoise from "simplex-noise"

type NoiseOpts = {
  scaleX?: number
  scaleY?: number
  iterations?: number
  persistence?: number
}

export abstract class Random {
  private static prng: prng | null = null
  private static simplex: SimplexNoise | null = null

  static seed(providedSeed: string): void {
    this.prng = seedrandom(providedSeed)
    this.simplex = new SimplexNoise(this.prng)
  }

  // Any random number in range [rangeStart, rangeEnd)
  static inRange(rangeStart: number, rangeEnd: number): number {
    if (rangeEnd < rangeStart) {
      throw new Error("Range end can't be smaller then range start")
    }

    if (this.prng == null) {
      throw new Error("Random needs to be seeded before it is called")
    }

    return rangeStart + this.prng() * (rangeEnd - rangeStart)
  }

  // Returns simplex2D noise value for point (x,y). Returned value is in range (-1, 1)
  static noise(
    x: number,
    y: number,
    { scaleX = 0.0015, scaleY = 0.0015, iterations = 6, persistence = 0.5 }: NoiseOpts = {}
  ): number {
    if (this.simplex == null) {
      throw new Error("Random needs to be seeded before it is called")
    }

    // This routine was inspired by code in blog post about simplex noise
    // https://cmaher.github.io/posts/working-with-simplex-noise/
    let maxAmplitude = 0
    let amplitude = 1
    let frequencyX = scaleX
    let frequencyY = scaleY
    let totalNoise = 0
    for (let i = 0; i < iterations; i++) {
      totalNoise += this.simplex.noise2D(x * frequencyX, y * frequencyY)
      maxAmplitude += amplitude
      amplitude *= persistence
      frequencyX *= 2
      frequencyY *= 2
    }

    return totalNoise / maxAmplitude
  }
}
