export enum Terrain {
  Forest = "Forest",
  Desert = "Desert",
  Water = "Water",
}

export enum Orientation {
  Top = "Cw180",
  Right = "Cw90",
  Bottom = "Cw0",
  Left = "Cw270",
}

export enum EntityType {
  PineSeedling = "PineSeedling",
  BirchSeedling = "BirchSeedling",
  MapleSeedling = "MapleSeedling",
  BlueberryBushSeedling = "BlueberryBushSeedling",

  PineHalfGrown = "PineHalfGrown",
  BirchHalfGrown = "BirchHalfGrown",
  MapleHalfGrown = "MapleHalfGrown",
  BlueberryBushHalfGrown = "BlueberryBushHalfGrown",

  PineMature = "PineMature",
  BirchMature = "BirchMature",
  MapleMature = "MapleMature",
  BlueberryBushMature = "BlueberryBushMature",

  StartingLocation = "StartingLocation", // Orientation will always be the same for now

  Slope = "Slope", // They are automatically placed upwards
}

// Create guard functions for trees and buildings and use them in exportMap
export const TreeEntityTypes = [
  EntityType.PineSeedling,
  EntityType.BirchSeedling,
  EntityType.MapleSeedling,
  EntityType.PineHalfGrown,
  EntityType.BirchHalfGrown,
  EntityType.MapleHalfGrown,
  EntityType.PineMature,
  EntityType.BirchMature,
  EntityType.MapleMature,
  // While blueberry bushes are not theoretically trees, for our use here they as well may be
  EntityType.BlueberryBushSeedling,
  EntityType.BlueberryBushHalfGrown,
  EntityType.BlueberryBushMature,
] as const
export type TreeEntityType = typeof TreeEntityTypes[number]

export const BuildingEntityTypes = [EntityType.StartingLocation, EntityType.Slope] as const
export type BuildingEntityType = typeof BuildingEntityTypes[number]
