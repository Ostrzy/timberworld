import { Random } from "../common/random"
import { EntityType, Terrain } from "../common/constants"

import { GenerationMap } from "./models"

const GROWTH_LEVELS = [0.2, 0.55]
const TREE_LEVELS = ["Seedling", "HalfGrown", "Mature"]

type TreeType = "Pine" | "Birch" | "Maple" | "BlueberryBush"

function randomTree(treeType: TreeType): EntityType {
  const randomGrowth = Random.inRange(0, 1)
  let noiseIndex = GROWTH_LEVELS.findIndex((level) => level > randomGrowth)
  if (noiseIndex === -1) {
    noiseIndex = GROWTH_LEVELS.length
  }
  return EntityType[`${treeType}${TREE_LEVELS[noiseIndex]}`]
}

interface TreesGenerationOpts {
  overrideProbability?: number
  noiseThreshold?: number
}

function generateTrees(
  gameMap: GenerationMap,
  treeType: TreeType,
  scaleX: number,
  scaleY: number,
  { overrideProbability = 0.5, noiseThreshold = 0.6 }: TreesGenerationOpts = {}
): void {
  gameMap.eachCell((cell, coords) => {
    if (cell.terrain === Terrain.Water) {
      return
    }

    if (cell.entity != null && Random.inRange(0, 1) >= overrideProbability) {
      return
    }

    const noise = Random.noise(coords.x, coords.y, { scaleX, scaleY, iterations: 3 })
    if (noise < noiseThreshold) {
      return
    }
    cell.entity = randomTree(treeType)
  })
}

export function generateForests(gameMap: GenerationMap): void {
  // Random ranges for the scales should give less of one-dimensional feeling
  generateTrees(gameMap, "Pine", Random.inRange(0.008, 0.012), Random.inRange(0.008, 0.012))
  generateTrees(gameMap, "Maple", Random.inRange(0.006, 0.014), Random.inRange(0.006, 0.014))
  generateTrees(gameMap, "Birch", Random.inRange(0.004, 0.016), Random.inRange(0.004, 0.016), {
    overrideProbability: 0.3,
    noiseThreshold: 0.6,
  })
  generateTrees(gameMap, "BlueberryBush", Random.inRange(0.03, 0.08), Random.inRange(0.03, 0.08), {
    overrideProbability: 0.1,
    noiseThreshold: 0.7,
  })
}
