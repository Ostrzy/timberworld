import { Random } from "../common/random"
import { Terrain } from "../common/constants"

import { GenerationMap } from "./models"

const NOISE_LEVELS = [-0.85, -0.05, 0.4, 0.65, 0.8, 0.9, 0.95]
const STARTING_HEIGHT = 3

function noiseToHeight(noise: number): number {
  let noiseIndex = NOISE_LEVELS.findIndex((level) => level > noise)
  if (noiseIndex === -1) {
    noiseIndex = NOISE_LEVELS.length
  }
  return STARTING_HEIGHT + noiseIndex
}

export function generateBaseTerrain(gameMap: GenerationMap): void {
  let minHeight: number = Infinity
  let anyWater: boolean = false

  gameMap.eachCell((cell, coords) => {
    const noise = Random.noise(coords.x, coords.y)
    cell.height = noiseToHeight(noise)

    minHeight = minHeight > cell.height ? cell.height : minHeight

    if (cell.height == STARTING_HEIGHT) {
      anyWater = true
      cell.height += 1
      cell.terrain = Terrain.Water
    }
  })

  // If there's no water on the map, we put water on all the lowest heights
  if (!anyWater) {
    gameMap.eachCell((cell) => {
      if (cell.height === minHeight) {
        cell.height += 1
        cell.terrain = Terrain.Water
      }
    })
  }
}
