import { GenerationMap } from "./models"
import { generateBaseTerrain } from "./generateBaseTerrain"
import { generateRivers } from "./generateRivers"
import { generateForests } from "./generateForests"

type GenerationOptions = {
  size: number
}

export function generateMap({ size }: GenerationOptions): GenerationMap {
  const gameMap = new GenerationMap(size)

  generateBaseTerrain(gameMap)
  generateRivers(gameMap)
  generateForests(gameMap)

  return gameMap
}

export { GenerationMap }
