import { CoordinatesSet, GenerationMap, Coordinates } from "./models"
import { Terrain, Orientation } from "../common/constants"
import { Random } from "../common/random"
import { C } from "./utils"

function collectLake(gameMap: GenerationMap, startingPoint: Coordinates): CoordinatesSet {
  const lake = new CoordinatesSet([startingPoint])
  const queue = [startingPoint]

  while (queue.length) {
    // We are sure there's at least one element cause thats what the loop checks for
    const currentCoordinates = queue.shift()!

    // Fetch all neighbors of current coordinates, ensuring they are real water coordinates, that
    // have not been yet added to lake.
    const unvisitedWaterNeighbors = currentCoordinates.neighbors
      .filter((coordinates) => gameMap.isWithin(coordinates))
      .filter((coordinates) => gameMap.getCell(coordinates).terrain === Terrain.Water)
      .filter((coordinates) => !lake.includes(coordinates))

    // Add the water neighbors to queue and the lake
    unvisitedWaterNeighbors.forEach((coordinates) => lake.add(coordinates))
    unvisitedWaterNeighbors.forEach((coordinates) => queue.push(coordinates))
  }

  return lake
}

function findConnectionFor(
  gameMap: GenerationMap,
  lake: CoordinatesSet,
  startingPoint: Coordinates | null = null,
  goal: Coordinates | null = null
): Coordinates {
  // If no starting point was provided, select some random point
  if (!startingPoint) {
    const lakeCoordinates = lake.coordinates
    const randomLakeIndex = Math.floor(Random.inRange(0, lakeCoordinates.length))
    startingPoint = lakeCoordinates[randomLakeIndex]
  }

  const startingCell = gameMap.getCell(startingPoint)
  const parents: { [coordinates: string]: Coordinates | null } = {
    [startingPoint.toString()]: null,
  }
  const processedCoordinates = new CoordinatesSet([startingPoint])
  const queue = [startingPoint]

  let droppedCoordinates = new CoordinatesSet()
  let height = startingCell.height

  let goalPoint: Coordinates | null = null

  while (!goalPoint && height < 20) {
    while (queue.length) {
      // We are sure there's at least one element cause thats what the loop checks for
      const currentCoordinates = queue.shift()!
      const currentCell = gameMap.getCell(currentCoordinates)

      const isGoalReached =
        goal && goal.x === currentCoordinates.x && goal.y === currentCoordinates.y
      const isNewLakeReached =
        currentCell.terrain === Terrain.Water && !lake.includes(currentCoordinates)
      if (isGoalReached || isNewLakeReached) {
        goalPoint = currentCoordinates
        break
      }

      // If current cell does not fullfil the condition, add its coordinates to dropped and continue
      if (currentCell.height > height) {
        droppedCoordinates.add(currentCoordinates)
        continue
      }

      // Fetch all neighbors of current coordinates, ensuring they are real coordinates on the map and
      // are still unvisited.
      const unvisitedNeighbors = currentCoordinates.neighbors
        .filter((coordinates) => gameMap.isWithin(coordinates))
        .filter((coordinates) => !processedCoordinates.includes(coordinates))

      // Add the unvisited neighbors to queue and assign currentCoordinates as their parent
      unvisitedNeighbors.forEach((coordinates) => {
        processedCoordinates.add(coordinates)
        queue.push(coordinates)
        parents[coordinates.toString()] = currentCoordinates
      })
    }

    queue.push(...droppedCoordinates.coordinates)
    droppedCoordinates = new CoordinatesSet()
    height += 1
  }

  if (!goalPoint) {
    throw new Error("No water found")
  }

  const waterCoordinates = new CoordinatesSet()
  let currentCoordinates: Coordinates | null = goalPoint
  let width = Random.inRange(2, 4)
  while (currentCoordinates != null) {
    const queue = [currentCoordinates]
    const viewedCoordinates = new CoordinatesSet([currentCoordinates])
    const iterations = Math.max(Math.min(width + Random.inRange(-0.4, 0.4), 4), 2)
    for (let i = 0; i < iterations; i++) {
      queue.splice(0, queue.length).forEach((coords) => {
        waterCoordinates.add(coords)
        const unvisitedNeighbors = coords.neighbors
          .filter((coordinates) => gameMap.isWithin(coordinates))
          .filter((coordinates) => !viewedCoordinates.includes(coordinates))
          .filter((coordinates) => {
            const cell = gameMap.getCell(coordinates)
            return cell.height <= height && cell.terrain !== Terrain.Water
          })
        unvisitedNeighbors.forEach((coordinates) => viewedCoordinates.add(coordinates))
        queue.push(...unvisitedNeighbors)
      })
    }
    currentCoordinates = parents[currentCoordinates.toString()]
  }
  waterCoordinates.coordinates.forEach((coords) => {
    const cell = gameMap.getCell(coords)
    cell.height = startingCell.height
    cell.terrain = Terrain.Water
    lake.add(coords)
  })

  return goalPoint
}

function randomPointOnEdge(mapSize: number, orientation: Orientation): Coordinates {
  const randomCoordinate = Math.floor(Random.inRange(0, mapSize))

  switch (orientation) {
    case Orientation.Left:
      return C(0, randomCoordinate)
    case Orientation.Right:
      return C(mapSize - 1, randomCoordinate)
    case Orientation.Bottom:
      return C(randomCoordinate, 0)
    case Orientation.Top:
      return C(randomCoordinate, mapSize - 1)
  }
}

export function generateRivers(gameMap: GenerationMap): void {
  const lakes: CoordinatesSet[] = []

  gameMap.eachCell((cell, coordinates) => {
    // If cell is not water cell, ignore it
    if (cell.terrain !== Terrain.Water) {
      return
    }

    // If cell is water, but is already included in one of the lakes, ignore it
    if (lakes.some((lake) => lake.includes(coordinates))) {
      return
    }

    // If cell is water cell and it is not yet added to any of the lakes, add a new lake that this
    // cell is part of.
    const newLake = collectLake(gameMap, coordinates)
    lakes.push(newLake)
  })

  while (lakes.length > 1) {
    const lake = lakes.shift()! // We can be sure there's at least one lake remaining
    const connectedLakeCoords = findConnectionFor(gameMap, lake)
    const connectedLakeIndex = lakes.findIndex((lake) => lake.includes(connectedLakeCoords))
    lake.merge(lakes[connectedLakeIndex])
    lakes.splice(connectedLakeIndex, 1)
    lakes.push(lake)
  }

  const mainLake = lakes[0]
  // Points closest to each map border. Initial points are supposed to be further then any actual
  // point on the map.
  const directions = {
    [Orientation.Left]: C(gameMap.size, gameMap.size),
    [Orientation.Right]: C(-1, -1),
    [Orientation.Bottom]: C(gameMap.size, gameMap.size),
    [Orientation.Top]: C(-1, -1),
  }

  mainLake.coordinates.forEach((coordinates) => {
    if (coordinates.x < directions[Orientation.Left].x) {
      directions[Orientation.Left] = coordinates
    }
    if (coordinates.x > directions[Orientation.Right].x) {
      directions[Orientation.Right] = coordinates
    }
    if (coordinates.y < directions[Orientation.Bottom].y) {
      directions[Orientation.Bottom] = coordinates
    }
    if (coordinates.y > directions[Orientation.Top].y) {
      directions[Orientation.Top] = coordinates
    }
  })

  const untouchedSides: Orientation[] = []
  if (directions[Orientation.Left].x !== 0) {
    untouchedSides.push(Orientation.Left)
  }
  if (directions[Orientation.Right].x !== gameMap.size - 1) {
    untouchedSides.push(Orientation.Right)
  }
  if (directions[Orientation.Bottom].y !== 0) {
    untouchedSides.push(Orientation.Bottom)
  }
  if (directions[Orientation.Top].y !== gameMap.size - 1) {
    untouchedSides.push(Orientation.Top)
  }

  if (untouchedSides.length > 2) {
    const additionalDirection = untouchedSides[Math.floor(Random.inRange(0, untouchedSides.length))]
    const startingPoint = directions[additionalDirection]
    const goal = randomPointOnEdge(gameMap.size, additionalDirection)
    findConnectionFor(gameMap, mainLake, startingPoint, goal)
  }
}
