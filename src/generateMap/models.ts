import { EntityType, Terrain, Orientation } from "../common/constants"
import { Pointable } from "../common/types"

import { C } from "./utils"

export class Coordinates implements Pointable {
  private readonly DIRECTIONS_MAP = {
    [Orientation.Top]: [0, 1],
    [Orientation.Right]: [1, 0],
    [Orientation.Bottom]: [0, -1],
    [Orientation.Left]: [-1, 0],
  }

  public readonly x: number
  public readonly y: number

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  neighbor(direction: Orientation): Coordinates {
    const [deltaX, deltaY] = this.DIRECTIONS_MAP[direction]
    return new Coordinates(this.x + deltaX, this.y + deltaY)
  }

  get neighbors(): Coordinates[] {
    return [
      Orientation.Top,
      Orientation.Right,
      Orientation.Bottom,
      Orientation.Left,
    ].map((direction) => this.neighbor(direction))
  }

  toString(): string {
    return `(${this.x},${this.y})`
  }
}

export class CoordinatesSet {
  private lookupSet: Set<string>
  private _coordinates: Coordinates[]

  constructor(initialCoordinates: Coordinates[] = []) {
    this.lookupSet = new Set()
    this._coordinates = []
    initialCoordinates.forEach((coordinates) => this.add(coordinates))
  }

  includes(coordinates: Coordinates): boolean {
    return this.lookupSet.has(coordinates.toString())
  }

  add(coordinates: Coordinates): void {
    if (this.includes(coordinates)) {
      return
    }
    this.lookupSet.add(coordinates.toString())
    this._coordinates.push(coordinates)
  }

  merge(anotherSet: CoordinatesSet): void {
    anotherSet.coordinates.forEach(this.add.bind(this))
  }

  get coordinates(): Coordinates[] {
    return this._coordinates.slice() // Create a copy to not mutate it accidentally
  }

  get length(): number {
    return this._coordinates.length
  }
}

export class MapCell {
  public height: number
  public terrain: Terrain
  public entity: EntityType | null

  constructor({
    height = 4,
    terrain = Terrain.Forest,
    entity = null,
  }: { height?: number; terrain?: Terrain; entity?: EntityType | null } = {}) {
    this.height = height
    this.terrain = terrain
    this.entity = entity
  }
}

export class GenerationMap {
  readonly MAX_SIZE = 512

  public readonly size: number
  private map: MapCell[][]

  constructor(size: number = 256) {
    if (size > this.MAX_SIZE) {
      throw Error(`Max map size is ${this.MAX_SIZE}. ${size} given.`)
    }

    this.size = size
    this.map = new Array(size)
    // Fill the two-dimensional map with basic Cells
    for (let i = 0; i < size; i++) {
      const row = new Array(size)
      for (let j = 0; j < size; j++) {
        row[j] = new MapCell()
      }
      this.map[i] = row
    }
  }

  eachCell(callback: (cell: MapCell, coordinates: Coordinates) => void): void {
    for (let y = 0; y < this.size; y++) {
      for (let x = 0; x < this.size; x++) {
        const coordinates = C(x, y)
        callback(this.getCell(coordinates), coordinates)
      }
    }
  }

  isWithin({ x, y }: Pointable) {
    if (x < 0 || y < 0) {
      return false
    }

    if (x >= this.size || y >= this.size) {
      return false
    }

    return true
  }

  getCell({ x, y }: Pointable): MapCell {
    if (!this.isWithin({ x, y })) {
      throw Error(`Coordinates (${x}, ${y}) are out of bounds for map of size ${this.size}`)
    }

    return this.map[x][y]
  }
}
