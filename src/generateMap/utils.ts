import { Coordinates } from "./models"

export function C(x: number, y: number) {
  return new Coordinates(x, y)
}
